
"""example.py: Use a maze and solve it with classic wall-following
   algorithm.

   The RL versions should be faster
"""

__author__    = "Pablo Alvarado"
__copyright__ = "Copyright 2020, Pablo Alvarado"
__license__   = "BSD 3-Clause License (Revised)"

import env

# maze = maze.Maze(40,40)
# maze.build()

# print(maze)
# maze.writeSVG('test.svg')

world = env.Environment(20,15,15)

world.run()
