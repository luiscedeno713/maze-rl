
"""maze.py: Maze """

__author__    = "Pablo Alvarado"
__copyright__ = "Copyright 2020, Pablo Alvarado"
__license__   = "BSD 3-Clause License (Revised)"

import random

class Cell:
    """A cell in the maze.

       A maze "Cell" is a point in the grid which may be surrounded by
       walls to the north, east, south or west.

       Based partially on the code at https://scipython.com/blog/making-a-maze/
       by Christian Hill, April 2017.
    """

    # A wall separates a pair of cells in the N-S or W-E directions.
    wall_pairs = {'N': 'S', 'S': 'N', 'E': 'W', 'W': 'E'}

    def __init__(self, x, y):
        """Initialize the cell at (x,y). At first it is surrounded by walls."""

        self.x, self.y = x, y
        self.walls = {'N': True, 'S': True, 'E': True, 'W': True}

    def hasAllWalls(self):
        """Does this cell still have all its walls?"""

        return all(self.walls.values())

    def deleteWall(self, other, wall):
        """Knock down the wall between cells self and other."""

        self.walls[wall] = False
        other.walls[Cell.wall_pairs[wall]] = False


class Maze:
    """A Maze, represented as a grid of cells."""

    def __init__(self, nx, ny, cellSize=15, ix=0, iy=0):
        """Initialize the maze grid.

           The maze consists of nx x ny cells and will be constructed
           starting at the cell indexed at (ix, iy).

        """

        self.nx, self.ny = nx, ny
        self.ix, self.iy = ix, iy
        self.cellSize = cellSize
        
        self.mazeMap = [[Cell(x, y) for x in range(self.nx)] for y in range(self.ny)]

    def cellAt(self, x, y):
        """Return the Cell object at (x,y)."""

        return self.mazeMap[y][x]

    def __str__(self):
        """Return a (crude) string representation of the maze."""

        mazeRows = ['-' * self.nx*2]
        for y in range(self.ny):
            mazeRow = ['|']
            for x in range(self.nx):
                if self.mazeMap[y][x].walls['E']:
                    mazeRow.append(' |')
                else:
                    mazeRow.append('  ')
            mazeRows.append(''.join(mazeRow))
            mazeRow = ['|']
            for x in range(self.nx):
                if self.mazeMap[y][x].walls['S']:
                    mazeRow.append('-+')
                else:
                    mazeRow.append(' +')
            mazeRows.append(''.join(mazeRow))
        return '\n'.join(mazeRows)

    def writeSVG(self, filename):
        """Write an SVG image of the maze to filename."""

        # Pad the maze all around by this amount.
        padding = 10
        # Height and width of the maze image (excluding padding), in pixels
        height = self.ny*self.cellSize + 1
        width  = self.nx*self.cellSize + 1
        
        # Scaling factors mapping maze coordinates to image coordinates
        scy, scx = self.cellSize,self.cellSize;

        def writeWall(f, x1, y1, x2, y2):
            """Write a single wall to the SVG image file handle f."""

            print('<line x1="{}" y1="{}" x2="{}" y2="{}"/>'
                                .format(x1, y1, x2, y2), file=f)

        # Write the SVG image file for maze
        with open(filename, 'w') as f:
            # SVG preamble and styles.
            print('<?xml version="1.0" encoding="utf-8"?>', file=f)
            print('<svg xmlns="http://www.w3.org/2000/svg"', file=f)
            print('    xmlns:xlink="http://www.w3.org/1999/xlink"', file=f)
            print('    width="{:d}" height="{:d}" viewBox="{} {} {} {}">'
                    .format(width+2*padding, height+2*padding,
                        -padding, -padding, width+2*padding, height+2*padding),
                  file=f)
            print('<defs>\n<style type="text/css"><![CDATA[', file=f)
            print('line {', file=f)
            print('    stroke: #000000;\n    stroke-linecap: square;', file=f)
            print('    stroke-width: 2;\n}', file=f)
            print(']]></style>\n</defs>', file=f)
            # Draw the "South" and "East" walls of each cell, if present (these
            # are the "North" and "West" walls of a neighbouring cell in
            # general, of course).
            for y in range(self.ny):
                for x in range(self.nx):
                    if self.cellAt(x,y).walls['S']:
                        x1, y1, x2, y2 = x*scx, (y+1)*scy, (x+1)*scx, (y+1)*scy
                        writeWall(f, x1, y1, x2, y2)
                    if self.cellAt(x,y).walls['E']:
                        x1, y1, x2, y2 = (x+1)*scx, y*scy, (x+1)*scx, (y+1)*scy
                        writeWall(f, x1, y1, x2, y2)
            # Draw the North and West maze border, which won't have been drawn
            # by the procedure above. 
            print('<line x1="0" y1="0" x2="{}" y2="0"/>'.format(width), file=f)
            print('<line x1="0" y1="0" x2="0" y2="{}"/>'.format(height),file=f)
            print('</svg>', file=f)

    def findValidNeighbors(self, cell):
        """Return a list of unvisited neighbours to cell."""

        delta = [('W', (-1,0)),
                 ('E', (1,0)),
                 ('S', (0,1)),
                 ('N', (0,-1))]
        neighbours = []
        for direction, (dx,dy) in delta:
            x2, y2 = cell.x + dx, cell.y + dy
            if (0 <= x2 < self.nx) and (0 <= y2 < self.ny):
                neighbour = self.cellAt(x2, y2)
                if neighbour.hasAllWalls():
                    neighbours.append((direction, neighbour))
        return neighbours

    def build(self):

        # Full cells
        self.mazeMap = [[Cell(x, y) for x in range(self.nx)] for y in range(self.ny)]

        
        # Total number of cells.
        n = self.nx * self.ny
        cellStack = []
        currentCell = self.cellAt(self.ix, self.iy)
        # Total number of visited cells during maze construction.
        nv = 1

        while nv < n:
            neighbours = self.findValidNeighbors(currentCell)

            if not neighbours:
                # We've reached a dead end: backtrack.
                currentCell = cellStack.pop()
                continue

            # Choose a random neighbouring cell and move to it.
            direction, nextCell = random.choice(neighbours)
            currentCell.deleteWall(nextCell, direction)
            cellStack.append(currentCell)
            currentCell = nextCell
            nv += 1

