
"""env.py: Environment configuration for the maze simulation"""

__author__    = "Pablo Alvarado"
__copyright__ = "Copyright 2020, Pablo Alvarado"
__license__   = "BSD 3-Clause License (Revised)"


import configparser
from PyQt5 import QtWidgets
import gui
import maze
import agent


class Environment:
    """"Environment class

        The environment holds a maze and one or more agents

    """
    
    def __init__(self, nx, ny,cellSize):
        self.guiRunning = False;
        self.maze = maze.Maze(nx,ny,cellSize)

    def render(self):
        print(self.maze)

        
    def run(self):
        if (not self.guiRunning):
            import sys
            app = QtWidgets.QApplication(sys.argv)
            MainWindow = QtWidgets.QMainWindow()
            ui = gui.Ui_MainWindow()
            ui.setupUi(MainWindow)
            ui.setEnvironment(self)
            MainWindow.show()
            ui.fitInCanvasView()
            self.guiRunning = True;
            sys.exit(app.exec_())
            self.guiRunning = False;
            
